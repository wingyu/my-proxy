FROM nginx
EXPOSE 80

COPY nginx.conf /etc/nginx/nginx.conf

#Configure reverse proxy
#RUN ln -nfs nginx.conf /etc/nginx/ && \
  #rm /etc/nginx/sites-enabled/default && \
  #service nginx restart
RUN service nginx restart

#Turn off when using compose
CMD nginx -g "daemon off;"

