USERNAME= wingyu
PROJECT = docker-nginx
TAG = 1.0
.PHONY: build, run

build:
	docker build --rm -t $(USERNAME)/$(PROJECT):$(TAG) .

run:
	docker run -d -it -p 80:80  $(USERNAME)/$(PROJECT):$(TAG)




